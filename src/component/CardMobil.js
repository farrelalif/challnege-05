import React from "react";
import { Button, Card } from "react-bootstrap";
import Icon from "react-hero-icon";




function CardMobil(props) {
  return (

    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={props.image} />
      <Card.Body>
        <Card.Title>Nama/Tipe Mobil</Card.Title>
        <Card.Text>
          {props.name}
          <h4>Rp {props.price} / hari</h4>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.{" "}
          <p><Icon className="mr-3" icon="users" type="outline" /> 4 orang{" "}</p>
          <p><Icon className="mr-3" icon="cog" type="outline" /> Manual{" "}</p>
          <p><Icon className="mr-3" icon="calendar" type="outline" /> Tahun 2020{" "}</p>
        </Card.Text>
        <Button variant="primary" onClick={props.detailCar}>Pilih Mobil</Button>
      </Card.Body>
    </Card>
  );
}

export default CardMobil;
